import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listStudent: null
    }
  }
  componentDidMount() {
    this.callAPi()
  }
  callAPi() {
    return new Promise((resolve, reject) => {
      const url = `http://localhost:8080/student`
      fetch(url, {
        method: "GET"
      })

        .then((response) => response.json())
        .then((res) => {
          this.setState({
            listStudent: res
          })
          return resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
  render() {
    let arr
    if (this.state.listStudent != null) {
      arr = this.state.listStudent.map((item, key) => {
        return (
          <div key={key} className="item">
            <div className="avatar">
              <img src={logo}/>
            </div>
            <div className="text">
              <h4>Thông tin cá nhân</h4>
              <p>Tên:   {item.name}</p>
              <p>Ngày sinh:   {item.dateOfBird}</p>
              <p>Giới tính:   {item.sex}</p>
              <p>Địa chỉ:   {item.address}</p>
            </div>

          </div>
        )
      })
    }
    return (
      <div>
        <div className="Title">
          <h3>Đây là front end viết bằng React cơ bản chưa sử dụng bất kì thư viện nào khác</h3>
        </div>
        <div className="listItem">
          {arr}
        </div>
      </div>
    )
  }
}

export default App;
